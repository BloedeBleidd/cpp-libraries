/*
 * fifo.h
 *
 *  Created on: Mar 28, 2020
 *      Author: BloedeBleidd - Piotr Zmuda - bloede.bleidd97@gmail.com
 */


#pragma once

#include <array>

template <typename Data, const uint32_t SIZE>
class Fifo
{
public:
	Fifo();
	bool push(const Data &);
	bool pop(Data &);
	void clear();
	uint32_t length() const;
	bool isEmpty() const;
	bool isFull() const;

private:
	std::array<Data, SIZE> buffer;
	uint32_t head;
	uint32_t tail;
	uint32_t capacity;
};

template <typename Data, const uint32_t SIZE>
Fifo<Data, SIZE>::Fifo()
{
	clear();
}

template <typename Data, const uint32_t SIZE>
bool Fifo<Data, SIZE>::push(const Data &data)
{
	if (capacity < SIZE) {
		uint32_t next = head+1;

		if (next >= SIZE) {
			next = 0;
		}
	
		buffer[head] = data;
		head = next;
		capacity++;

		return true;
	}

	return false;
}

template <typename Data, const uint32_t SIZE>
bool Fifo<Data, SIZE>::pop(Data &data)
{
	if (capacity) {
		uint32_t next = tail+1;

		if(SIZE <= next) {
			next = 0;
		}

		data = buffer[tail];
		tail = next;
		capacity--;

		return true;
	}

	return false;
}

template <typename Data, const uint32_t SIZE>
void Fifo<Data, SIZE>::clear()
{
	head = 0;
	tail = 0;
	capacity = 0;
}

template <typename Data, const uint32_t SIZE>
uint32_t Fifo<Data, SIZE>::length() const
{
	return capacity;
}

template <typename Data, const uint32_t SIZE>
bool Fifo<Data, SIZE>::isEmpty() const
{
	return !capacity;
}

template <typename Data, const uint32_t SIZE>
bool Fifo<Data, SIZE>::isFull() const
{
	return SIZE == capacity;
}
